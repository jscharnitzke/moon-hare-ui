export class ProductFilter {
  brandNames: string[];
  tags: string[];
  minPrice: number;
  maxPrice: number;

  constructor() {
    this.brandNames = [];
    this.tags = [];
    this.minPrice = 0;
    this.maxPrice = 0;
  }

  isActive(): boolean {
    return (
      this.brandNames.length > 0 ||
      this.tags.length > 0 ||
      this.minPrice > 0 ||
      this.maxPrice > 0
    );
  }
}
