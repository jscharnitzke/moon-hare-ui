import { Component, OnInit, Input } from '@angular/core';
import {
  MatCheckboxChange,
  MatAutocompleteSelectedEvent
} from '@angular/material';
import { Catalog } from '../catalog.model';
import { ProductFilter } from './product-filter.model';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  private productFilter: ProductFilter;

  catalogPlaceholder: Catalog;

  @Input()
  set catalog(catalog: Catalog) {
    this.catalogPlaceholder = catalog;
  }

  get catalog() {
    return this.catalogPlaceholder;
  }

  filteredTags: Observable<string[]>;
  filteredBrandNames: Observable<string[]>;
  minPrice: number;
  maxPrice: number;

  tagControl = new FormControl();
  brandNameControl = new FormControl();
  minPriceControl = new FormControl();
  maxPriceControl = new FormControl();

  constructor() {}

  ngOnInit() {
    this.productFilter = new ProductFilter();
    this.minPrice = 0;
    this.maxPrice = 0;

    this.filteredTags = this.tagControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterTags(value))
    );

    this.filteredBrandNames = this.brandNameControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterBrandNames(value))
    );
  }

  onFilter(type: string, event?: MatAutocompleteSelectedEvent) {
    switch (type) {
      case 'brandName':
        this.brandNameControl.setValue('');
        this.productFilter.brandNames.push(event.option.value);
        break;
      case 'tag':
        this.tagControl.setValue('');
        this.productFilter.tags.push(event.option.value);
        break;
      case 'price':
        this.productFilter.minPrice = this.minPriceControl.value;
        this.productFilter.maxPrice = this.maxPriceControl.value;
        break;
    }

    this.catalog.productFilter = this.productFilter;
  }

  onFilterRemoved(type: string, value: string | number) {
    switch (type) {
      case 'brandName':
        this.productFilter.brandNames = this.productFilter.brandNames.filter(
          filteredBrandName => filteredBrandName !== value
        );
        break;
      case 'tag':
        this.productFilter.tags = this.productFilter.tags.filter(
          filteredTag => filteredTag !== value
        );
        break;
    }
    this.catalog.productFilter = this.productFilter;
  }

  private _filterTags(value: string): string[] {
    const filterValue = value.toLowerCase();

    return value.length > 0
      ? this.catalog.uniqueTags.filter(tag =>
          tag.toLowerCase().includes(filterValue)
        )
      : this.catalog.uniqueTags;
  }

  private _filterBrandNames(value: string): string[] {
    const filterValue = value.toLowerCase();

    return value.length > 0
      ? this.catalog.uniqueBrandNames.filter(tag =>
          tag.toLowerCase().includes(filterValue)
        )
      : this.catalog.uniqueBrandNames;
  }
}
