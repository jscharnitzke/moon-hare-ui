import { Component, OnInit } from '@angular/core';
import { StoreService } from './store.service';
import { Catalog } from './catalog.model';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  catalog: Catalog;

  loading = true;

  constructor(private productService: StoreService) {}

  async ngOnInit() {
    this.catalog = new Catalog();
    this.catalog.products = await this.productService.fetchProducts();
    this.loading = false;
  }

  getImageUrl(imagePath: string) {
    return `url(${imagePath})`;
  }
}
