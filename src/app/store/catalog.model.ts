import { Product } from './product/product.model';
import { ProductFilter } from './filters/product-filter.model';

import cloneDeep from 'lodash/cloneDeep';
import orderBy from 'lodash/orderBy';

export class Catalog {
  private _products: Product[];
  private _filteredProducts: Product[];
  private _productFilter: ProductFilter;

  uniqueBrandNames: string[];
  uniqueTags: string[];

  constructor() {
    this._products = [];
    this._filteredProducts = [];
    this._productFilter = new ProductFilter();

    this.uniqueBrandNames = [];
    this.uniqueTags = [];
  }

  get products(): Product[] {
    return this._filteredProducts.length > 0 || this._productFilter.isActive()
      ? cloneDeep(this._filteredProducts)
      : cloneDeep(this._products);
  }

  set products(products: Product[]) {
    this._products = products.map(product => Object.create(product));

    this.uniqueBrandNames = products
      .map(product => product.brandName)
      .filter((el, i, a) => i === a.indexOf(el))
      .sort();

    this.uniqueTags = products
      .map(product => [...product.tags])
      .reduce((acc, tags) => acc.concat(tags), [])
      .filter((el, i, a) => i === a.indexOf(el))
      .sort();
  }

  get productFilter() {
    return Object.create(this._productFilter);
  }

  set productFilter(productFilter: ProductFilter) {
    this._productFilter = Object.create(productFilter);
    this._filteredProducts = this._products.filter(
      product =>
        (this._productFilter.brandNames.length === 0 ||
          this._productFilter.brandNames.includes(product.brandName)) &&
        (this._productFilter.tags.length === 0 ||
          this._productFilter.tags.every(tag => product.tags.includes(tag))) &&
        (this._productFilter.maxPrice === 0 ||
          product.price <= this._productFilter.maxPrice) &&
        product.price >= this._productFilter.minPrice
    );
  }

  sort(sortFields: Map<string, string>) {
    this._products = orderBy(this._products, [...sortFields.keys()], [
      ...sortFields.values()
    ] as ReadonlyArray<boolean | 'asc' | 'desc'>);

    this._filteredProducts = orderBy(
      this._filteredProducts,
      [...sortFields.keys()],
      [...sortFields.values()] as ReadonlyArray<boolean | 'asc' | 'desc'>
    );
  }
}
