import { ProductVariant } from './product-variant.model';

export class Product {
  id: number;
  type: string;
  name: string;
  description: string;
  price: number;
  brandName: string;
  image: string;
  variants: ProductVariant[];

  protected _tags: string[];

  constructor() {
    this.type = 'perfume';
    this.name = 'Placeholder Name';
    this.description = 'Placeholder Description';
    this.price = 0;
    this.brandName = 'Placeholder Brand Name';
    this.image = '/assets/product-placeholder.jpg';
    this.variants = [];
  }

  get tags() {
    return this._tags ? [...this._tags] : null;
  }

  set tags(tags: string[]) {
    this._tags = [...tags];
  }
}
