import { Component, OnInit, EventEmitter } from '@angular/core';
import { Product } from './product.model';
import { StoreService } from '../store.service';
import { ActivatedRoute } from '@angular/router';
import {
  MatSelect,
  MatSelectChange
} from '../../../../node_modules/@angular/material';
import { ProductInventory } from './product-inventory.model';

@Component({
  selector: 'app-products',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  selectedSku: ProductInventory = new ProductInventory();

  product: Product;
  inventory: ProductInventory[];

  constructor(
    private storeService: StoreService,
    private route: ActivatedRoute
  ) {}

  async ngOnInit() {
    this.product = new Product();
    this.product.id = this.route.snapshot.params.productId;

    [this.product, this.inventory] = await Promise.all([
      this.storeService.fetchPerfume(this.product.id),
      this.storeService.fetchInventory(this.product.id)
    ]);
  }

  addToCart(sku: ProductInventory) {
    this.storeService.addToCart(sku);
  }
}
