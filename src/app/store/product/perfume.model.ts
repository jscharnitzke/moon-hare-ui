import { Product } from './product.model';

export class Perfume extends Product {
  volume: number;

  private _scentNotes: string[];

  constructor(perfume?: Perfume) {
    super();
    this.type = 'perfume';

    if (perfume) {
      this.id = perfume.id;
      this.name = perfume.name;
      this.brandName = perfume.brandName;
      this.description = perfume.description;
      this.volume = perfume.volume;
      this.scentNotes = perfume.scentNotes;
      this.price = perfume.price;
      this.image = perfume.image;

      console.log(typeof this.price);
    }
  }

  get scentNotes() {
    return this._scentNotes ? Object.create(this._scentNotes) : null;
  }

  set scentNotes(scentNotes: string[]) {
    this._scentNotes = Object.create(scentNotes);
  }

  get tags() {
    return this._tags
      ? this._scentNotes
        ? [...this._tags, ...this._scentNotes]
        : Object.create(this._tags)
      : this._scentNotes
        ? Object.create(this._scentNotes)
        : null;
  }
}
