export class ProductVariant {
  sku: string;

  protected _characteristics: Map<string, string>;

  constructor() {
    this.sku = '';
    this._characteristics = new Map();
  }

  get characteristics() {
    return new Map(this._characteristics);
  }

  set characteristics(characteristics: Map<string, string>) {
    this._characteristics = new Map(characteristics);
  }
}
