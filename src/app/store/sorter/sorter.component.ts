import { Component, OnInit, Input } from '@angular/core';
import { Catalog } from '../catalog.model';
import { MatSelectChange } from '@angular/material';

@Component({
  selector: 'app-sorter',
  templateUrl: './sorter.component.html',
  styleUrls: ['./sorter.component.scss']
})
export class SorterComponent implements OnInit {
  private _sortFields: Map<string, string>;

  catalogPlaceholder: Catalog;

  @Input()
  set catalog(catalog: Catalog) {
    this.catalogPlaceholder = catalog;
  }

  get catalog() {
    return this.catalogPlaceholder;
  }

  constructor() {}

  ngOnInit() {
    this._sortFields = new Map<string, string>();
  }

  sort(type: string, event: MatSelectChange) {
    event.value.length !== 0
      ? this._sortFields.set(type, event.value)
      : this._sortFields.delete(type);

    this.catalog.sort(this._sortFields);
  }
}
