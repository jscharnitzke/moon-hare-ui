import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Perfume } from './product/perfume.model';
import { Product } from './product/product.model';
import { Subject } from 'rxjs';
import { ProductInventory } from './product/product-inventory.model';

@Injectable()
export class StoreService {
  private api = environment.api;
  private cart: ProductInventory[];

  addedToCart = new Subject<ProductInventory[]>();

  constructor(private http: HttpClient) {
    this.cart = [];
  }

  /**
   * Fetch all products in the store.
   *
   * @returns {Promise<Product[]>} A Promise that will resolve to an array of all current products.
   * @memberof StoreService
   */
  async fetchProducts(): Promise<Product[]> {
    return new Promise<Product[]>(async (resolve, reject) => {
      const perfumes = await this.fetchPerfumes();

      resolve([...perfumes]);
    });
  }

  /**
   * Fetch a specific perfume's details.
   *
   * @param {number} perfumeId The ID of the perfume.
   * @returns {Promise<Perfume>} A Promise that will resolve to the perfume's details.
   * @memberof StoreService
   */
  fetchPerfume(perfumeId: number): Promise<Perfume> {
    return new Promise(resolve => {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });

      this.http
        .get(`${this.api}/perfumes?id=${perfumeId}`, { headers: headers })
        .subscribe((response: Perfume) => {
          resolve(response[0]);
        });
    });
  }

  /**
   * Fetch all perfumes in the store.
   *
   * @private
   * @returns {Promise<Perfume[]>} A Promise that will resolve to an array of all current perfumes.
   * @memberof StoreService
   */
  private fetchPerfumes(): Promise<Perfume[]> {
    return new Promise<Perfume[]>(resolve => {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });

      this.http
        .get(`${this.api}/perfumes`, { headers: headers })
        .subscribe((response: Perfume[]) => {
          const perfumes: Perfume[] = [];
          for (const perfume of response) {
            perfumes.push(new Perfume(perfume));
          }

          resolve(perfumes);
        });
    });
  }

  async fetchInventory(productId: number): Promise<ProductInventory[]> {
    return new Promise<ProductInventory[]>(async resolve => {
      const inventory: ProductInventory[] = [];
      const sizes = ['Sample', 'Dram', 'Full'];
      const product = await this.fetchPerfume(productId);

      for (const size of sizes) {
        const sku = new ProductInventory();
        sku.size = size;
        sku.inStock = Math.floor(Math.random() * 6);
        sku.sku = (
          product.brandName.slice(0, 3) +
          '-' +
          product.name.replace(' ', '').slice(0, 8) +
          '-' +
          size.slice(0, 2)
        ).toUpperCase();

        inventory.push(sku);
      }

      resolve(inventory);
    });
  }

  /**
   * Add the selected item to the user's cart.
   *
   * @param {Product} product The product to add to the cart.
   * @memberof StoreService
   */
  addToCart(product: ProductInventory) {
    this.cart.push(product);
    this.addedToCart.next(this.cart);
  }
}
