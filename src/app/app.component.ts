import { Component, OnInit } from '@angular/core';
import { StoreService } from './store/store.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: string;
  cartCount: number;

  constructor(
    private storeService: StoreService,
    private snackBar: MatSnackBar
  ) {
    this.title = 'Moon & Hare';
    this.cartCount = 0;
  }

  ngOnInit() {
    this.storeService.addedToCart.subscribe(cart => {
      this.cartCount = cart.length;
      this.snackBar.open('Item added to cart', 'Close', {
        duration: 4000
      });
    });
  }
}
