import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { StoreComponent } from './store/store.component';
import { StoreService } from './store/store.service';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FiltersComponent } from './store/filters/filters.component';
import { AppRoutingModule } from './app-routing.module';
import { ProductComponent } from './store/product/product.component';
import { SorterComponent } from './store/sorter/sorter.component';

@NgModule({
  declarations: [
    AppComponent,
    StoreComponent,
    FiltersComponent,
    ProductComponent,
    SorterComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    AppRoutingModule
  ],
  providers: [StoreService],
  bootstrap: [AppComponent]
})
export class AppModule {}
