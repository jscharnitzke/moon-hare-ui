import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatToolbarModule,
  MatCardModule,
  MatGridListModule,
  MatChipsModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatInputModule,
  MatIconModule,
  MatSelectModule,
  MatButtonModule,
  MatSnackBarModule,
  MatBadgeModule,
  MatProgressBarModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, BrowserAnimationsModule],
  exports: [
    MatToolbarModule,
    MatCardModule,
    MatGridListModule,
    MatChipsModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
    MatBadgeModule,
    MatProgressBarModule
  ],
  declarations: []
})
export class MaterialModule {}
